# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Class(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    Class_Name=models.CharField(max_length=100,blank=True )
    class Meta:
        ordering = ['created']

    def __str__(self):
        return self.Class_Name    

  
     

class Subject(models.Model):
    Sub_Name=models.CharField(max_length=100,blank=True)
    created = models.DateTimeField(auto_now_add=True)
    class Meta:
        ordering = ['created']
    def __str__(self):
        return self.Sub_Name  
        
    
class Teacher(models.Model):
    Standard = models.ForeignKey('Class_app.Class',related_name='Class_app1',on_delete=models.CASCADE)
    Teacher=models.ForeignKey('User_app.User',related_name='User_app1',on_delete=models.CASCADE)
    subject=models.ForeignKey('Class_app.Subject',related_name='Class_app2',on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    class Meta:
        ordering = ['created']

    def __str__(self):
        template= '{0.Standard} {0.Teacher}{0.subject}'
        return template.format(self) 


    # def save(self, *args, **kwargs):

    #     super(Teacher, self).save()   
  

