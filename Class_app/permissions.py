from django.contrib.auth.models import Group
from rest_framework import permissions
from User_app.models import User
from .models import Class
from .models import Subject
from .models import Teacher
from django.contrib.auth import get_user_model
User = get_user_model()

class IsAdminUser(permissions.BasePermission):
    #import pdb; pdb.set_trace();
    def has_permission(self, request, view):
        user_grp = User.objects.filter(username=request.user.username).values_list('groups__name', flat=True)
        print(user_grp)
        if 'Admin' in user_grp:
            return True

class IsAdminOrTeacherUser(permissions.BasePermission):
    
    def has_permission(self, request, view):
        user_grp = User.objects.filter(username=request.user.username).values_list('groups__name', flat=True)
        print(user_grp)
        if 'Admin' in user_grp :
            return True
    
        elif  'Teacher' in user_grp and request.method == 'GET':
            return True