from django.urls import path, include
from rest_framework.routers import DefaultRouter
from Class_app import views
#from django.conf.urls import include

# Create a router and register our viewsets with it.
router = DefaultRouter()
router.register(r'classes', views.ClassViewSet)
router.register(r'teachers', views.TeacherViewSet)
router.register(r'subjects', views.SubjectViewSet)
# The API URLs are now determined automatically by the router.
urlpatterns = [
    path('class_app', include(router.urls)),
]