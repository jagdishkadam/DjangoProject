# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Class 
from .models import Teacher
from .models import Subject
admin.site.register(Class)
admin.site.register(Teacher)
admin.site.register(Subject)

# Register your models here.
