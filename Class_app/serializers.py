from rest_framework import serializers
from Class_app.models import Class
from Class_app.models import Teacher
from Class_app.models import Subject
from django.db import models

class ClassSerializer(serializers.ModelSerializer):
    class Meta:
        model = Class
        fields='__all__'
class TeacherSerializer(serializers.ModelSerializer):
    class Meta:
        model = Teacher
        fields='__all__'
        depth = 1 

    # def __str__(self):
    #     template= '{0.Standard} {0.Teacher}{0.subject}'
    #     return template.format(self)  

class TeacherTestSerializer(serializers.ModelSerializer):
    class Meta:
        model = Teacher
        fields='__all__'
           
                    
class SubjectSerializer(serializers.ModelSerializer):
    Sub_Name=serializers.CharField(max_length=100)
    #can remove bellow class of user if don't want it.
    class Meta:
        model = Subject
        fields='__all__'
         


        
                     