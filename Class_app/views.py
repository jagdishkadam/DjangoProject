# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from rest_framework import viewsets
from Class_app.models import Class
from Class_app.serializers import ClassSerializer
from Class_app.models import Teacher
from Class_app.serializers import TeacherSerializer
from Class_app.serializers import TeacherTestSerializer
from Class_app.models import Subject
from Class_app.serializers import SubjectSerializer
from Class_app.permissions import IsAdminUser,IsAdminOrTeacherUser
#from rest_framework.response import Response
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework import permissions
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
User = get_user_model()

# Create your views here.
class ClassViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for viewing and editing .
    """
    permission_classes = (IsAdminUser,)
    queryset = Class.objects.all()
    serializer_class = ClassSerializer
    #permission_classes = [IsAccountAdminOrReadOnly]

class TeacherViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for viewing and editing .
    """
    permission_classes = (IsAdminOrTeacherUser,)
    queryset = Teacher.objects.all()
    serializer_class = TeacherSerializer
    def get_serializer_class(self):
        if self.request.method in ['POST','PUT','PATCH']:
            serializer_class = TeacherTestSerializer
            return TeacherTestSerializer
        return TeacherSerializer
    def get_queryset(self):
        queryset = Teacher.objects.all()
        user_grp_t = User.objects.filter(id=self.request.user.id).values_list('groups__name',flat=True)
        if 'Admin'in user_grp_t:
            return Teacher.objects.all()
        elif 'Teacher' in user_grp_t:
            return queryset.filter(Teacher=self.request.user)    
class SubjectViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for viewing and editing .
    """
    permission_classes = (IsAdminUser,)
    queryset = Subject.objects.all()
    serializer_class = SubjectSerializer    