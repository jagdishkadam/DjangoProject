from django.contrib.auth.models import Group
from rest_framework import permissions
#from .models import User
from django.contrib.auth import get_user_model
User = get_user_model()

class IsAdminUser(permissions.BasePermission):
    def has_permission(self, request, view):
        user_grp = User.objects.filter(username=request.user).values_list('groups__name', flat=True)
        if 'Admin' in user_grp:
            return True

class IsAdminOrStudentUser(permissions.BasePermission):
    def has_permission(self, request, view):
        user_grp = User.objects.filter(username=request.user).values_list('groups__name', flat=True)
        if 'Admin' in user_grp:
            return True
        elif  'Student' in user_grp and request.method == 'GET':
            return True