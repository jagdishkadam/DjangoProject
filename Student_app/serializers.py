from rest_framework import serializers
from Student_app.models import Student
from django.db import models

class StudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields='__all__'
        depth = 1
        

class StudentTestSerializer(serializers.ModelSerializer):
    
    # def __str__(self):
    #     template= '{0.Standard} {0.Student}{0.subject}'
    #     return template.format(self)    
    #can remove bellow class of user if don't want it.
    class Meta:
        model = Student
        fields='__all__'
       
      