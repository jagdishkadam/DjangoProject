# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User

from django.db import models
class Student(models.Model):
    Standard=models.ForeignKey('Class_app.Class',related_name='Class2',on_delete=models.CASCADE)
    Student=models.ForeignKey('User_app.User',related_name='User2',on_delete=models.CASCADE)
    subject=models.ForeignKey('Class_app.Subject',related_name='Class3',on_delete=models.CASCADE)
    created=models.DateTimeField(auto_now_add=True)
    class Meta:
        ordering = ['created']

    def __str__(self):
        template= '{0.Standard} {0.Student}{0.subject}'
        return template.format(self)    


    

