# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework import viewsets
from django.shortcuts import render
from Student_app.models import Student
from Student_app.serializers import StudentSerializer
from Student_app.permissions import IsAdminUser, IsAdminOrStudentUser
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
User = get_user_model()
from Student_app.serializers import StudentTestSerializer
# Create your views here.
class StudentViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for viewing and editing.
    """
    permission_classes = [IsAdminOrStudentUser]
    queryset = Student.objects.all()
    serializer_class = StudentSerializer

    def get_serializer_class(self):
        if self.request.method in ['POST', 'PUT','PATCH']:
            serializer_class = StudentTestSerializer
            return StudentTestSerializer
        return StudentSerializer
    def get_queryset(self):
        user_grp_s = User.objects.filter(id=self.request.user.id).values_list('groups__name', flat=True)
        queryset = Student.objects.all()
        if 'Admin' in user_grp_s:
            #import pdb; pdb.set_trace();
            queryset = Student.objects.all()
            return Student.objects.all()
        elif 'Student' in user_grp_s:
            return queryset.filter(Student=self.request.user)   