FROM python:latest
ENV  NODE_ENV=production
RUN mkdir /code
WORKDIR /code
RUN pip3 install django && pip3 install djangorestframework 
RUN pip3 install psycopg2 
COPY . /code/

EXPOSE 8006
EXPOSE 5432
CMD ["python", "./StudentManager/manage.py", "runserver", "0.0.0.0:8006" ] && ["python", "./StudentManager/manage.py", "migrate"] 