**StudentManager**


This project has been created for student management where user having different roles .user can be student ,teacher & admin.

1)__Install docker__:
                  sudo apt-get update
                  sudo apt-get remove docker docker-engine docker.io
                  sudo apt-get install docker-ce
                  sudo systemctl start docker 
                  sudo systemctl enable docker
2)__To run project__:
                  sudo docker-compose build 
                  sudo docker-compose up

3)__To apply migration__ :
     check CONTAINER_ID : sudo docker ps
     enter in conatiner of project image not in postgres image:sudo docker exec -it CONTAINER_ID bash 
     in container's root ,go in project directory:python3 manage.py makemigartions
                                                  python3 manage.py migarte 
                                                  python3 manage.py createsuperuser --username test

4)__hit the url__ : localhost:8006
5)go in admin panel & create groups (Student,Teacher&Admin) & create users.

__permissions__:
1. If User is Admin then it can call all the APIs & read data ,create data, update data & delete data.
2. If User is Teacher then he can call only User API & read data. Also he can view his own data in       teacher api to which standard he is teaching for which subject.
3. If User is Student then he can call only User API & read data. Also he can view his own data in       student api in which class he is for which subject.

                   
                  


 