# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)

class User(AbstractUser):
    created = models.DateTimeField(auto_now_add=True)
    Full_Name=models.CharField(max_length=1000,blank=True)
    #owner = models.ForeignKey('auth.User',related_name='User1',on_delete=models.CASCADE)
    class Meta:
        ordering = ['created']

    def __str__(self):
        return self.Full_Name 






