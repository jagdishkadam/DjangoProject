from rest_framework import serializers
from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from django.contrib.auth import get_user_model

User = get_user_model()
#from django.contrib.auth.hashers import make_password

class GroupSerializer(serializers.ModelSerializer): 
    
    class Meta:
        model = Group
        fields = '__all__'

class UserSerializer(serializers.ModelSerializer):    
    class Meta:
        model = User
        fields=('Full_Name','groups','username', 'password',)
        extra_kwargs={'password':{'write_only':True}}
    
  

    
     