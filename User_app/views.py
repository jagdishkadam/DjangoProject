 # -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from rest_framework import viewsets
from User_app.models import User
from django.contrib.auth.models import Group
from User_app.serializers import UserSerializer, GroupSerializer
from django.contrib.auth.decorators import login_required
from rest_framework.views import APIView
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.response import Response
from User_app.permissions import IsAdminUser
from User_app.permissions import IsAdminOnly

# Create your views here.
class GroupViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for viewing and editing.
    """
    permission_classes = [IsAdminOnly]
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
class UserViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for viewing and editing.
    """
    permission_classes = (IsAdminUser,)
    queryset = User.objects.all()
    serializer_class = UserSerializer



    def perform_create(self, serializer):
        instance = serializer.save()
        instance.set_password(instance.password)
        instance.save()
    def perform_update(self, serializer):
        instance = serializer.save()
        instance.set_password(instance.password)
        instance.save()

    

         

       
   
        
        
   
    