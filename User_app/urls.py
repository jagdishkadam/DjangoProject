from django.urls import path,include
from rest_framework.routers import DefaultRouter
from User_app import views
#from django.conf.urls import include

# Create a router and register our viewsets with it.
router = DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)
# The API URLs are now determined automatically by the router.

urlpatterns = [
    path('user_app', include(router.urls)),
]
